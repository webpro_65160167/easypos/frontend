import type { Product } from '@/types/Product'
import http from './http'

function addProduct(product: Product & { files: File[] }) {
  const fromData = new FormData()
  fromData.append('name', product.name)
  fromData.append('price', product.price.toString())
  fromData.append('type', JSON.stringify(product.type))
  if(product.files && product.files.length > 0) fromData.append('file', product.files[0])
  return http.post('/products', fromData, {
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  })
}

function updateProduct(product: Product & { files: File[] }) {
  const fromData = new FormData()
  fromData.append('name', product.name)
  fromData.append('price', product.price.toString())
  fromData.append('type', JSON.stringify(product.type))
  if(product.files && product.files.length > 0) fromData.append('file', product.files[0])
  return http.post(`/products/${product.id}`, fromData, {
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  })
}

function delProduct(product: Product) {
  return http.delete(`/products/${product.id}`)
}

function getProduct(id: number) {
  return http.get(`/products/${id}`)
}

function getProducts() {
  return http.get('/products')
}

function getProductsByType(typeId: number) {
  return http.get('/products/type/'+ typeId)
}

export default { addProduct, updateProduct, delProduct, getProduct, getProducts, getProductsByType }
