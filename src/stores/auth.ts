import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { User } from '@/types/User'
import authService from '@/services/auth'
import { useMessageStore } from './massage'
import {  useRouter } from 'vue-router'
import { useLoadingStore } from './loading'

export const useAuthStore = defineStore('auth', () => {
  const messageStore = useMessageStore()
  const router = useRouter()
  const loadingStore = useLoadingStore()
  const login = async function(email: string, password: string) {
    loadingStore.doLoad()
    try{
      const res = await authService.login(email, password)
      console.log(res.data);
      localStorage.setItem('user', JSON.stringify(res.data.user))
      localStorage.setItem('access_token', res.data.access_token)
      messageStore.showMessage('Login success')
      router.push('/')
    } catch(e: any) {
      console.log(e.message);
      messageStore.showMessage(e.message)
    }
    loadingStore.finish()
  }

  const logout = function() {
    localStorage.removeItem('user')
    localStorage.removeItem('access_token')
    router.push('/login')
  }

  function getCurrentUser(): User | null {
    const strUser = localStorage.getItem('user')
    if(strUser === null) return null
    return JSON.parse(strUser)
  }

  function getToken(): string | null {
    const strToken = localStorage.getItem('access_token')
    if(strToken === null) return null
    return JSON.parse(strToken)
  }
  return { getCurrentUser, login, getToken, logout }
})
