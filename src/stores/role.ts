import { useLoadingStore } from './loading'
import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import roleService from '@/services/role'
import type { Role } from '@/types/Role'

export const useRoleStore = defineStore('role', () => {
  const loadingStore = useLoadingStore()
  const roles = ref<Role[]>([])
  const initialRole: Role = {
    name: ''
  }
  const editedRole = ref<Role>(JSON.parse(JSON.stringify(initialRole)))

  async function getRole(id: number) {
    loadingStore.doLoad()
    const res = await roleService.getRole(id)
    editedRole.value = res.data
    loadingStore.finish()
  }
  async function getRoles() {
    try{
      loadingStore.doLoad()
      const res = await roleService.getRoles()
      roles.value = res.data
      loadingStore.finish()
    } catch(e) {
      loadingStore.finish()
    }
  }
  async function saveRole() {
    loadingStore.doLoad()
    const role = editedRole.value
    if (!role.id) {
      // Add new
      console.log('Post ' + JSON.stringify(role))
      const res = await roleService.addRole(role)
    } else {
      // Update
      console.log('Patch ' + JSON.stringify(role))
      const res = await roleService.updateRole(role)
    }

    await getRoles()
    loadingStore.finish()
  }
  async function deleteRole() {
    loadingStore.doLoad()
    const role = editedRole.value
    const res = await roleService.delRole(role)

    await getRoles()
    loadingStore.finish()
  }

  function clearForm() {
    editedRole.value = JSON.parse(JSON.stringify(initialRole))
  }
  return { roles, getRoles, saveRole, deleteRole, editedRole, getRole, clearForm }
})
